package Util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class CookieSave {
    public void Save(HttpServletResponse response,String name,String value,int last_time)
    {
        //创建Cookie，将用户名存到叫cookieUserName的cookie中
        //Cookie 对象携带需要保存的数据，user.getName()=value，都是字符串类型
        //每个cookie保存一个数据，如果需要多个，创建多个cookie对象
        Cookie cookieUserName = new Cookie(name, value);
        //设置cookie存在时间   单位：秒
        //cookie保存的时间，不管中途是否使用，访问cookie，到时就过期
        //如果不设置，那么cookie在浏览器关闭的时候失效
        cookieUserName.setMaxAge(last_time);
        //将cookie发给浏览器（如果没有这句，cookie就不会发送给客户端）
        response.addCookie(cookieUserName);
    }
}
