package Entity;


import java.sql.Date;

public class News {
    private int idnews=0;
    private String category="";
    private String title="";
    private String content="";
    private String author="";
    private Date newsdate;

    public int getIdnews() {
        return idnews;
    }
    public void setIdnews(int idnews) {
        this.idnews = idnews;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public Date getNewsdate() {
        return newsdate;
    }
    public void setNewsdate(Date newsdate) {
        this.newsdate = newsdate;
    }

}
