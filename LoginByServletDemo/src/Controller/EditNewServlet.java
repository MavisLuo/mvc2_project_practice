package Controller;

import Entity.News;
import Service.NewsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
/*
编辑新闻
 */
@WebServlet(name = "EditServlet")
public class EditNewServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        int idnews=Integer.valueOf(request.getParameter("idnews"));
        NewsService newsService=new NewsService();
        try {
            News news=newsService.GetNews(idnews);
            String temp=htmlToString(news.getContent());
            news.setContent(temp);
            request.setAttribute("news",news);
            //request.getRequestDispatcher("AddNews.jsp").forward(request,response);
            request.getRequestDispatcher("EditNews.jsp").forward(request,response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static String htmlToString(String str)
    {
        if(str==null) return "";
        if(str.equals("")) return "";
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("&amp;amp;", "&amp;");
        str = str.replaceAll("&amp;quot;", "&quot;");
        str = str.replaceAll("\"", "&quot;");
        str = str.replaceAll("&amp;lt;", "&lt;");
        str = str.replaceAll("&amp;gt;", "&gt;");
        str = str.replaceAll("&amp;nbsp;", "&nbsp;");
        return str;
    }
}
