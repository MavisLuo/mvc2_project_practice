package Controller;

import Service.NewsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/*
删除新闻
 */
@WebServlet(name = "DeleteServlet")
public class DeleteNewServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        int idnews=Integer.valueOf(request.getParameter("idnews"));
        System.out.println(request.getParameter("idnews"));
        NewsService newsService=new NewsService();
        newsService.DeleteNews(idnews);
        request.getRequestDispatcher("ShowNewsListServlet").forward(request,response);
    }
}
