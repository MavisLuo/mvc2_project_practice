<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script>
        function getCookie(cname){
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i].trim();
                if (c.indexOf(name)==0) { return c.substring(name.length,c.length); }
            }
            return "";
        }
        function checkCookie(){
            var user=getCookie("username");
            var uname=document.getElementById("username");
            var pwd=getCookie("password");
            var upwd=document.getElementById("password");

            if (user!=""&&pwd!=""){
                uname.value=user;
                upwd.value=pwd;
            }
        }
        window.onload=function()//用window的onload事件，窗体加载完毕的时候
        {
            checkCookie();
        }
    </script>
</head>

<body>
    <div class="t">
        <div class="h">
            <div class="logo"><img src="images/login_logo.png" /></div>
            <a href="" target="_blank" class="help">帮助</a>
        </div>
    </div>
    <div class="c">
        <div class="box" style="right:60px; top:60px;" id="box">
            <ul class="tab" id="tab">
                <li class="current">帐号登录</li>
                <li class="dragbar" id="dragbar"></li>
            </ul>
            <!-- 账号密码登录-->
            <div class="boxc" id="tab_via_acct">
                <h3>用户登录</h3>
                <form name='form_login' method="post" action="LoginServlet">
                    <div class="text_item">
                        <input type="text" class="text" id="username" name="username" style="ime-mode:disabled" autocomplete="off" placeholder="用户名" onfocus="this.className='text_f'" onblur="this.className='text'" />
                        <div class="pop" style="display:none;" id="pop"></div>
                    </div>
                    <div class="text_item">
                        <input type="hidden" class="text" name="user" value="" />
                        <input type="password" class="text" id="password" name="password" placeholder="密码" onfocus="this.className='text_f'" onblur="this.className='text'" />
                    </div>
                    <div class="bl">
                        <span style="float:left">
                            <input type="hidden" name="login_ssl" value="0" />
                            <p id="tip" style="color:red;font-family:宋体;clear: both;"></p> 
                        </span>
                        <span class="blt">
                            <a href="" target="_self">忘记密码</a>                    
                        </span>
                    </div>
                    <div class="btnb">
                        <!-- 登录按钮 -->
                        <input type="submit" name="submit" class="btn" id="btnLogin" value="登  录" style="float:right"/>
                        <div style="clear:both"></div>
                    </div>
                </form>
            </div>
        </div>
        <div class="f" id="f" style="display:none;"></div>
        <div class="login_drag" id="drag_target"></div>
    </div>
    <div class="b">西南石油大学</div>
</body>

</html>