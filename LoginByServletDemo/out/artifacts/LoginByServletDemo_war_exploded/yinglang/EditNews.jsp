<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>修改新闻</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <script type="text/javascript" charset="utf-8" src="ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="ueditor.all.min.js"> </script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.min.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="lang/zh-cn/zh-cn.js">
        window.UEDITOR_HOME_URL = "${basePath}"
    </script>
    <style type="text/css">
        div{
            width:100%;
        }
    </style>
    <link rel="stylesheet" href="js/bootstrap.min.css">
</head>
<body>
</div>
<div style="padding-top: 20px;padding-right: 10%;padding-left: 10%">
    <form action="SaveEditNewServlet" method="post">
        <input type="hidden" id="idnews" name="idnews" value="${news.idnews}"/>
        <div class="form-group">
            <label for="title">标题</label>
            <input type="text" class="form-control" id="title" placeholder="title" name="title" value="${news.title}">
        </div>
        <div class="form-group">
            <label for="category">类别</label>
            <input type="text" class="form-control" id="category" placeholder="category" name="category" value="${news.category}">
        </div>
        <div class="form-group">
            <label for="author">作者</label>
            <input type="text" class="form-control" id="author" placeholder="author" name="author" value="${news.author}">
        </div>
        <div class="form-group">
            <label for="newsdate">时间</label>
            <input type="text" class="form-control" id="newsdate" placeholder="newsdate" name="newsdate" value="${news.newsdate}">
        </div>
        <input type="hidden" id="content" name="content" value="${news.content}"/>
        <button type="button" class="btn btn-success" onclick="addcontent(this.form)">提交修改</button>
        <div class="form-group">
            <label for="editor">内容</label>
            <div>
                <script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    function addcontent(form) {
        document.getElementById("content").value=getContent();
        form.submit();
    }


    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var ue = UE.getEditor('editor');

    ue.ready(function() {
        insertHtml((document.getElementById("content").value));
    });

    function isFocus(e){
        alert(UE.getEditor('editor').isFocus());
        UE.dom.domUtils.preventDefault(e)
    }
    function setblur(e){
        UE.getEditor('editor').blur();
        UE.dom.domUtils.preventDefault(e)
    }
    function insertHtml(value) {
        //var value = prompt('插入html代码', '');
        alert(value);
        UE.getEditor('editor').execCommand('insertHtml', value)
    }
    function createEditor() {
        enableBtn();
        UE.getEditor('editor');
    }
    function getAllHtml() {
        alert(UE.getEditor('editor').getAllHtml())
    }
    function getContent() {
        var arr = [];
       // arr.push("使用editor.getContent()方法可以获得编辑器的内容");
        //arr.push("内容为：");
        arr.push(UE.getEditor('editor').getContent());
        //alert(arr);
        return arr;
    }
    function getPlainTxt() {
        var arr = [];
        //arr.push("使用editor.getPlainTxt()方法可以获得编辑器的带格式的纯文本内容");
        //arr.push("内容为：");
        arr.push(UE.getEditor('editor').getPlainTxt());
        //alert(arr.join('\n'))
        return arr;
    }
    function setContent(isAppendTo,str_value) {
        var arr = [];
        //arr.push("使用editor.setContent('欢迎使用ueditor')方法可以设置编辑器的内容");
        UE.getEditor('editor').setContent(str_value, isAppendTo);
        //alert(arr.join("\n"));
    }
    function setDisabled() {
        UE.getEditor('editor').setDisabled('fullscreen');
        disableBtn("enable");
    }

    function setEnabled() {
        UE.getEditor('editor').setEnabled();
        enableBtn();
    }

    function getText() {
        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
        var range = UE.getEditor('editor').selection.getRange();
        range.select();
        var txt = UE.getEditor('editor').selection.getText();
        alert(txt)
    }

    function getContentTxt() {
        var arr = [];
        arr.push("使用editor.getContentTxt()方法可以获得编辑器的纯文本内容");
        arr.push("编辑器的纯文本内容为：");
        arr.push(UE.getEditor('editor').getContentTxt());
        alert(arr.join("\n"));
    }
    function hasContent() {
        var arr = [];
        arr.push("使用editor.hasContents()方法判断编辑器里是否有内容");
        arr.push("判断结果为：");
        arr.push(UE.getEditor('editor').hasContents());
        alert(arr.join("\n"));
    }
    function setFocus() {
        UE.getEditor('editor').focus();
    }
    function deleteEditor() {
        disableBtn();
        UE.getEditor('editor').destroy();
    }
    function disableBtn(str) {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            if (btn.id == str) {
                UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
            } else {
                btn.setAttribute("disabled", "true");
            }
        }
    }
    function enableBtn() {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
        }
    }

    function getLocalData () {
        alert(UE.getEditor('editor').execCommand( "getlocaldata" ));
    }

    function clearLocalData () {
        UE.getEditor('editor').execCommand( "clearlocaldata" );
        alert("已清空草稿箱")
    }
</script>
</body>
</html>